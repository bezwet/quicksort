package test;

import main.QuickSort;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class QuickSortTest {

    @ParameterizedTest(name = "Array before sorting: {0}, expected array after sorting: {1}.")
    @MethodSource("quickSort")
    public void quickSort_test(int[] input, int[] result) {
        QuickSort quickSort = new QuickSort();

        quickSort.sort(input, 0, input.length - 1);
        assertArrayEquals(input, result);
    }

    public static Stream<Arguments> quickSort() {
        return Stream.of(
                Arguments.of(new int[]{}, new int[]{}),
                Arguments.of(new int[]{10, 5, 2, 2}, new int[]{2, 2, 5, 10}),
                Arguments.of(new int[]{-1, 5, -20, 20}, new int[]{-20, -1, 5, 20}),
                Arguments.of(new int[]{0, 0, -100, 200}, new int[]{-100, 0, 0, 200}));
    }
}
