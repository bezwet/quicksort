package main;

public class Main {
    public static void main(String[] args) {
        int[] array = {8, 4, 2, 3, 10, 11, 5};
        System.out.println("Before sort:");
        print(array);

        QuickSort quickSort = new QuickSort();
        quickSort.sort(array, 0, array.length - 1);

        System.out.println("After sort:");
        print(array);
    }

    public static void print(int[] arr) {
        for (int j : arr) {
            System.out.print(j + ",");
        }
        System.out.println();
    }
}