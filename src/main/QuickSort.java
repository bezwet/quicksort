package main;

public class QuickSort {
    public void sort(int[] array, int lowIndex, int highIndex) {
        if (array == null || array.length == 0) {
            return;
        }
        if (lowIndex >= highIndex) {
            return;
        }
        int pivot = array[highIndex];
        int leftPointer = lowIndex;
        int rightPointer = highIndex;

        while (leftPointer < rightPointer) {

            while (leftPointer < rightPointer && pivot >= array[leftPointer]) {
                leftPointer++;
            }

            while (leftPointer < rightPointer && pivot <= array[rightPointer]) {
                rightPointer--;
            }
            swap(array, leftPointer, rightPointer);
        }
        swap(array, highIndex, leftPointer);

        sort(array, lowIndex, leftPointer - 1);
        sort(array, leftPointer + 1, highIndex);
    }

    public static void swap(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
